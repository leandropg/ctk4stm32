/**
 *  @file ctk4stm32.h
 *  @brief CT4STM32 C Header
 *  @date 11/04/2017
 *  @version 1.0.0
 *
 *  C Toolkit For STM32 Microcontrollers
 *  Copyright (C) 2017  Leandro Perez Guatibonza
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CTK4STM32_H_
#define CTK4STM32_H_

#include "stm32f4xx_hal.h"

/**
 * Low Level
 */
#define LOW 			0

/**
 * High Level
 */
#define HIGH 			1

/**
 * Led and Button STM32 Nucleo Board
 */
#define LED 			LED_GPIO_Port,LED_Pin
#define BUTTON 			BUTTON_GPIO_Port,BUTTON_Pin

/*
 * LCD Pin Definition
 */
#define LCD_RS			LCD_RS_GPIO_Port,LCD_RS_Pin
#define LCD_E			LCD_E_GPIO_Port,LCD_E_Pin
#define LCD_D4			LCD_D4_GPIO_Port,LCD_D4_Pin
#define LCD_D5			LCD_D5_GPIO_Port,LCD_D5_Pin
#define LCD_D6			LCD_D6_GPIO_Port,LCD_D6_Pin
#define LCD_D7			LCD_D7_GPIO_Port,LCD_D7_Pin

/**
 * LCD Clear Constant
 */
#define LCD_CLEAR 		0x01
/**
 * LCD Blink On Constant
 */
#define LCD_BLINK_ON 	0x0D
/**
 * LCD Blink Off Constant
 */
#define LCD_BLINK_OFF 	0x0C
/**
 * LCD Cursor On Constant
 */
#define LCD_CURSOR_ON 	0x0E
/**
 * LCD Cursor Off Constant
 */
#define LCD_CURSOR_OFF 	0x0C
/**
 * LCD Display On Constant
 */
#define LCD_DISPLAY_ON 	0x0C
/**
 * LCD Display Off Constant
 */
#define LCD_DISPLAY_OFF 0x08
/**
 * LCD Display Off Constant
 */
#define LCD_SHIFT_OFF 	0x10
/**
 * LCD Shift Left Constant
 */
#define LCD_SHIFT_LEFT	0x18
/**
 * LCD Shift Right Constant
 */
#define LCD_SHIFT_RIGHT	0x1C
/**
 * LCD Home Constant
 */
#define LCD_HOME  		0x80

/**
 * @brief Delay in Miliseconds
 * @param delayMs Milisecond Value
 */
void delayMs(uint32_t delayMs);

/**
 * @brief Init LCD Module
 */
void lcdInit();

/**
 * @brief Send Byte to Two Nibbles
 * @param data Byte to send to LCD Module
 */
void lcdData(__uint8_t data);

/**
 * @brief Send Instruction LCD Module
 * @param instru Instruction to send to LCD Module
 */
void lcdInstru(__uint8_t instru);

/**
 * @brief Clear LCD Module
 */
void lcdClear();

/**
 * @brief Set Home Cursor Position LCD Module
 */
void lcdHome();

/**
 * @brief Set Cursor Position LCD Module
 * @param row LCD Row
 * @param column LCD Column
 */
void lcdSetCursor(__uint8_t row, __uint8_t column);

/**
 * @brief Write Character LCD Module
 * @param exportData Data to Send to LCD Module
 */
void lcdWrite(__uint8_t character);

/**
 * @brief Write Character LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 */
void lcdWriteSetPosition(__uint8_t row, __uint8_t column, __uint8_t exportData);

/**
 * @brief Write Character LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param message Array Message to Send to LCD Module
 */
void lcdWriteMessage(__uint8_t row, __uint8_t column, const __uint8_t message []);

/**
 * @brief Set Cursor Blink On LCD Module
 */
void lcdCursorBlinkOn();

/**
 * @brief Set Cursor Blink Off LCD Module
 */
void lcdCursorBlinkOff();

/**
 * @brief Set Cursor Display On LCD Module
 */
void lcdCursorDisplayOn();

/**
 * @brief Set Cursor Display Off LCD Module
 */
void lcdCursorDisplayOff();

/**
 * @brief Set Display On LCD Module
 */
void lcdDisplayOn();

/**
 * @brief Set Display Off LCD Module
 */
void lcdDisplayOff();

/**
 * @brief Set Shift Off LCD Module
 */
void lcdShiftOff();

/**
 * @brief Set Shift Left LCD Module
 */
void lcdShiftLeft();

/**
 * @brief Set Shift Right LCD Module
 */
void lcdShiftRight();

/**
 * @brief Send Data Binary Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 */
void lcdDataBinFormat(__uint8_t row, __uint8_t column, __uint8_t dataExport);

/**
 * @brief Send Data Hexadecimal Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 */
void lcdDataHexFormat(__uint8_t row, __uint8_t column, __uint8_t dataExport);

/**
 * @brief Send Data Decimal Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 */
void lcdDataDecFormatSetPosition(__uint8_t row, __uint8_t column, __uint16_t dataExport, __uint8_t quantityDigits);

/**
 * @brief Send Data Decimal Format LCD Module
 * @param dataExport Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 */
void lcdDataDecFormat(__uint16_t dataExport, __uint8_t quantityDigits);

/**
 * @brief Send Data Double Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param dataExport Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 * @param quantityDecimals Quantity Decimals
 */
void lcdDataDoubleFormatSetPosition(__uint8_t row, __uint8_t column, double dataExport, __uint8_t quantityDigits, __uint8_t quantityDecimals);

/**
 * @brief Send Data Double Format LCD Module
 * @param dataExport Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 * @param quantityDecimals Quantity Decimals
 */
void lcdDataDoubleFormat(double dataExport, __uint8_t quantityDigits, __uint8_t quantityDecimals);

/**
 * @brief Send Data Date Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param day Day
 * @param month Month
 * @param year Year
 */
void lcdDataDateFormat(__uint8_t row, __uint8_t column, __uint16_t day, __uint16_t month, __uint16_t year);

/**
 * @brief Send Data Time Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param hour Hour
 * @param minute Minute
 * @param second Second
 */
void lcdDataTimeFormat(__uint8_t row, __uint8_t column, __uint16_t hour, __uint16_t minute, __uint16_t second);

/**
 * @brief Read Digital Pin
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 * @return Value Digital Read
 */
unsigned char pinDigitalRead(GPIO_TypeDef* gpioPort, uint16_t gpioPin);

/**
 * @brief Write Digital Pin On
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 */
void pinDigitalWriteOn(GPIO_TypeDef* gpioPort, uint16_t gpioPin);

/**
 * @brief Write Digital Pin Off
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 */
void pinDigitalWriteOff(GPIO_TypeDef* gpioPort, uint16_t gpioPin);

/**
 * @brief Write Digital Toggle Pin
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 */
void pinDigitalWriteToggle(GPIO_TypeDef* gpioPort, uint16_t gpioPin);

#endif
