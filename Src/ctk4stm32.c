/**
 *  @file ctk4stm32.c
 *  @brief CT4STM32 C Code
 *  @date 11/04/2017
 *  @version 1.0.0
 *
 *  C Toolkit For STM32 Microcontrollers
 *  Copyright (C) 2017  Leandro Perez Guatibonza
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "ctk4stm32.h"

/**
 * @brief Delay in Miliseconds
 * @param delayMs Milisecond Value
 */
void delayMs(uint32_t delayMs) {

	HAL_Delay(delayMs);
}

/**
 * @brief Init LCD Module
 */
void lcdInit() {
	// Idle State
	pinDigitalWriteOff(LCD_RS);
	pinDigitalWriteOff(LCD_E);
	delayMs(15);

	// Init 4-bit LCD Mode
	pinDigitalWriteOn(LCD_E);
	pinDigitalWriteOff(LCD_D7);
	pinDigitalWriteOff(LCD_D6);
	pinDigitalWriteOn(LCD_D5);
	pinDigitalWriteOn(LCD_D4);
	pinDigitalWriteOff(LCD_E);
	delayMs(5);
	pinDigitalWriteOn(LCD_E);
	pinDigitalWriteOff(LCD_E);
	delayMs(1);
	pinDigitalWriteOn(LCD_E);
	pinDigitalWriteOff(LCD_E);
	delayMs(5);
	pinDigitalWriteOn(LCD_E);
	pinDigitalWriteOff(LCD_D4);
	pinDigitalWriteOff(LCD_E);
	delayMs(1);

	// LCD 4-bit Interface
	lcdInstru(0x28);

	// LCD Off Display
	lcdInstru(0x08);

	// LCD On Display
	lcdInstru(0x0C);

	// LCD Message Static with cursor auto-increment
	lcdInstru(0x06);

	// LCD Cursor Home
	lcdInstru(0x02);

	// LCD Cursor Clear
	lcdInstru(0x01);
}

/**
 * @brief Send Byte to Two Nibbles
 * @param byteExport Byte to send to LCD Module
 */
void lcdSendByte(__uint8_t byteExport) {

	__uint8_t bitExport = 0;

	// Send 4-bit MSB
	pinDigitalWriteOn(LCD_E);

	bitExport = byteExport & 0x80;
	if(bitExport == 0x80) {

		pinDigitalWriteOn(LCD_D7);

	} else {

		pinDigitalWriteOff(LCD_D7);
	}

	bitExport = byteExport & 0x40;
	if(bitExport == 0x40){

		pinDigitalWriteOn(LCD_D6);
	} else {

		pinDigitalWriteOff(LCD_D6);
	}

	bitExport = byteExport & 0x20;
	if(bitExport == 0x20) {

		pinDigitalWriteOn(LCD_D5);

	} else {

		pinDigitalWriteOff(LCD_D5);
	}

	bitExport = byteExport & 0x10;
	if(bitExport == 0x10) {

		pinDigitalWriteOn(LCD_D4);

	} else {

		pinDigitalWriteOff(LCD_D4);
	}
	pinDigitalWriteOff(LCD_E);

	// Send 4-bit LSB
	pinDigitalWriteOn(LCD_E);

	bitExport = byteExport & 0x08;
	if(bitExport == 0x08) {

		pinDigitalWriteOn(LCD_D7);

	} else {

		pinDigitalWriteOff(LCD_D7);
	}

	bitExport = byteExport & 0x04;
	if(bitExport == 0x04) {

		pinDigitalWriteOn(LCD_D6);

	} else {

		pinDigitalWriteOff(LCD_D6);
	}

	bitExport = byteExport & 0x02;
	if(bitExport == 0x02) {

		pinDigitalWriteOn(LCD_D5);

	} else {

		pinDigitalWriteOff(LCD_D5);
	}

	bitExport = byteExport & 0x01;
	if(bitExport == 0x01) {

		pinDigitalWriteOn(LCD_D4);

	} else {

		pinDigitalWriteOff(LCD_D4);
	}
	pinDigitalWriteOff(LCD_E);

	// Delay 2 ms
	delayMs(2);
}

/**
 * @brief Send Data LCD Module
 */
void lcdData(__uint8_t data) {

	// Data Mode
	pinDigitalWriteOn(LCD_RS);

	// Send Byte
	lcdSendByte(data);
}

/**
 * @brief Send Instruction LCD Module
 * @param instru Instruction to send to LCD Module
 */
void lcdInstru(__uint8_t instru) {

	// Instruction Mode
	pinDigitalWriteOff(LCD_RS);

	// Send Byte
	lcdSendByte(instru);
}

/**
 * @brief Clear LCD Module
 */
void lcdClear() {

	lcdInstru(LCD_CLEAR);
}

/**
 * @brief Set Home Cursor Position LCD Module
 */
void lcdHome() {

	lcdInstru(LCD_HOME);
}

/**
 * @brief Set Cursor Position LCD Module
 * @param row LCD Row
 * @param column LCD Column
 */
void lcdSetCursor(__uint8_t row, __uint8_t column) {

	__uint8_t lcdPosition = 0;

	if(row > 0 && row <= 4 && column > 0 && column <= 20) {

		if(row == 1){

			lcdPosition = 0x80 + (column - 1);

		} else if(row == 2) {

			lcdPosition = 0xC0 + (column - 1);

		} else if(row == 3) {

			lcdPosition = 0x94 + (column - 1);

		} else if(row == 4) {

			lcdPosition = 0xD4 + (column - 1);
		}

		// Send position to LCD Module
		lcdInstru(lcdPosition);
	}
}

/**
 * @brief Write Character LCD Module
 * @param exportData Data to Send to LCD Module
 */
void lcdWrite(__uint8_t exportData) {

	lcdData(exportData);
}

/**
 * @brief Write Character LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 */
void lcdWriteSetPosition(__uint8_t row, __uint8_t column, __uint8_t exportData) {

	// LCD Position
	lcdSetCursor(row, column);

	// Write Data LCD Module
	lcdData(exportData);
}

/**
 * @brief Write Character LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param message Array Message to Send to LCD Module
 */
void lcdWriteMessage(__uint8_t row, __uint8_t column, const __uint8_t message []) {

	int i = 0;

	// LCD Position
	lcdSetCursor(row, column);

	while(message[i] != 0) {

		lcdData(message[i]);

		// Next Character
		i++;
	}
}

/**
 * @brief Set Cursor Blink On LCD Module
 */
void lcdCursorBlinkOn() {

	lcdInstru(LCD_BLINK_ON);
}

/**
 * @brief Set Cursor Blink Off LCD Module
 */
void lcdCursorBlinkOff() {

	lcdInstru(LCD_BLINK_OFF);
}

/**
 * @brief Set Cursor Display On LCD Module
 */
void lcdCursorDisplayOn() {

	lcdInstru(LCD_CURSOR_ON);
}

/**
 * @brief Set Cursor Display Off LCD Module
 */
void lcdCursorDisplayOff() {

	lcdInstru(LCD_CURSOR_OFF);
}

/**
 * @brief Set Display On LCD Module
 */
void lcdDisplayOn() {

	lcdInstru(LCD_DISPLAY_ON);
}

/**
 * @brief Set Display Off LCD Module
 */
void lcdDisplayOff() {

	lcdInstru(LCD_DISPLAY_OFF);
}

/**
 * @brief Set Shift Off LCD Module
 */
void lcdShiftOff() {

	lcdInstru(LCD_SHIFT_OFF);
}

/**
 * @brief Set Shift Left LCD Module
 */
void lcdShiftLeft() {

	lcdInstru(LCD_SHIFT_LEFT);
}

/**
 * @brief Set Shift Right LCD Module
 */
void lcdShiftRight() {

	lcdInstru(LCD_SHIFT_RIGHT);
}

/**
 * @brief Send Data Binary Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 */
void lcdDataBinFormat(__uint8_t row, __uint8_t column, __uint8_t exportData) {

	int i = 0;
	__uint8_t binaryMask = 0x80;
	__uint8_t digit = 0;

	// LCD Position
	lcdSetCursor(row, column);

	for(i=0; i < 8; i++) {

		// Get Digit
		digit = (exportData / binaryMask);

		// Fix the Export Data and Binary Mask
		if(digit == 1){

			exportData = exportData - binaryMask;
		}
		binaryMask = binaryMask >> 1;

		// Export Digit to LCD Module
		digit += 0x30;
		lcdData(digit);
	}

	// Print Binary Symbol
	lcdData('b');
}

/**
 * @brief Send Data Hexadecimal Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 */
void lcdDataHexFormat(__uint8_t row, __uint8_t column, __uint8_t exportData) {

	__uint8_t firstDigit = 0;
	__uint8_t secondDigit = 0;

	// LCD Position
	lcdSetCursor(row, column);

	// Print Hex Symbol
	lcdData('0');
	lcdData('x');

	// First Digit
	firstDigit = exportData / 16;

	// Second Digit
	secondDigit = exportData - (firstDigit * 16);

	// Export First Digit
	if(firstDigit > 9) {

		firstDigit += 0x37;

	} else {

		firstDigit += 0x30;
	}
	lcdData(firstDigit);

	// Export Second Digit
	if(secondDigit > 9) {

		secondDigit += 0x37;

	} else {

		secondDigit += 0x30;
	}
	lcdData(secondDigit);
}

/**
 * @brief Send Data Decimal Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param exportData Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 */
void lcdDataDecFormatSetPosition(__uint8_t row, __uint8_t column, __uint16_t exportData, __uint8_t quantityDigits) {

	// LCD Position
	lcdSetCursor(row, column);

	// Send Data Decimal Format LCD Module
	lcdDataDecFormat(exportData, quantityDigits);
}

/**
 * @brief Send Data Decimal Format LCD Module
 * @param dataExport Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 */
void lcdDataDecFormat(__uint16_t dataExport, __uint8_t quantityDigits) {

	__uint8_t i = 0;
	__uint16_t divisor = 1;
	__uint8_t digitExport = 0;

	// Get Divisor
	for(i = 1; i < quantityDigits; i++) {

		divisor *= 10;
	}

	// Get and Export Digits
	for(i = 1; i <= quantityDigits; i++) {

		// Export Digit
		digitExport = (dataExport / divisor) + 0x30;
		lcdData(digitExport);

		// Decrement export data and divisor
		dataExport %= divisor;
		divisor /= 10;
	}
}

/**
 * @brief Send Data Double Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param dataExport Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 * @param quantityDecimals Quantity Decimals
 */
void lcdDataDoubleFormatSetPosition(__uint8_t row, __uint8_t column, double dataExport, __uint8_t quantityDigits, __uint8_t quantityDecimals) {

	// LCD Position
	lcdSetCursor(row, column);

	// Send Data Double Format LCD Module
	lcdDataDoubleFormat(dataExport, quantityDigits, quantityDecimals);
}

/**
 * @brief Send Data Double Format LCD Module
 * @param dataExport Data to Send to LCD Module
 * @param quantityDigits Quantity Digits
 * @param quantityDecimals Quantity Decimals
 */
void lcdDataDoubleFormat(double dataExport, __uint8_t quantityDigits, __uint8_t quantityDecimals) {

	__uint8_t i = 0;
	__uint16_t divisorDigits = 1;
	__uint16_t divisorDecimals = 1;
	__uint16_t digitPart = 0;
	__uint16_t decimalPart = 0;
	__uint8_t digitExport = 0;

	// Get Divisor Digits
	for(i = 1; i < quantityDigits; i++) {

		divisorDigits *= 10;
	}

	// Get Divisor Decimals
	for(i = 1; i < quantityDecimals; i++) {

		divisorDecimals *= 10;
	}

	// Separate digit and part
	digitPart = (__uint8_t) dataExport;

	// Separate decimal part
	dataExport -= digitPart;
	dataExport *= (divisorDecimals * 10);
	decimalPart = (__uint8_t) dataExport;

	// Get and Export Digits
	for(i = 1; i <= quantityDigits; i++) {

		// Export Digit
		digitExport = (digitPart / divisorDigits) + 0x30;
		lcdData(digitExport);

		// Decrement export data and divisor
		digitPart %= divisorDigits;
		divisorDigits /= 10;
	}

	lcdWrite('.');

	// Get and Export Decimals
	for(i = 1; i <= quantityDecimals; i++) {

		// Export Digit
		digitExport = (decimalPart / divisorDecimals) + 0x30;
		lcdData(digitExport);

		// Decrement export data and divisor
		decimalPart %= divisorDecimals;
		divisorDecimals /= 10;
	}
}

/**
 * @brief Send Data Date Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param day Day
 * @param month Month
 * @param year Year
 */
void lcdDataDateFormat(__uint8_t row, __uint8_t column, __uint16_t day, __uint16_t month, __uint16_t year) {

	// LCD Position
	lcdSetCursor(row, column);

	// Write Day
	lcdDataDecFormat(day,2);
	lcdWrite('/');

	// Write Month
	lcdDataDecFormat(month,2);
	lcdWrite('/');

	// Write Day
	lcdDataDecFormat(year,4);
}

/**
 * @brief Send Data Time Format LCD Module
 * @param row LCD Row
 * @param column LCD Column
 * @param hour Hour
 * @param minute Minute
 * @param second Second
 */
void lcdDataTimeFormat(__uint8_t row, __uint8_t column, __uint16_t hour, __uint16_t minute, __uint16_t second) {

	// LCD Position
	lcdSetCursor(row, column);

	// Write Day
	lcdDataDecFormat(hour,2);
	lcdWrite(':');

	// Write Month
	lcdDataDecFormat(minute,2);
	lcdWrite(':');

	// Write Day
	lcdDataDecFormat(second,2);
}

/**
 * @brief Read Digital Pin
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 * @return Value Digital Read
 */
GPIO_PinState pinDigitalRead(GPIO_TypeDef* gpioPort, uint16_t gpioPin) {

	return HAL_GPIO_ReadPin(gpioPort, gpioPin);
}

/**
 * @brief Write Digital Pin On
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 */
void pinDigitalWriteOn(GPIO_TypeDef* gpioPort, uint16_t gpioPin) {

	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_SET);
}

/**
 * @brief Write Digital Pin Off
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 */
void pinDigitalWriteOff(GPIO_TypeDef* gpioPort, uint16_t gpioPin) {

	HAL_GPIO_WritePin(gpioPort, gpioPin, GPIO_PIN_RESET);
}

/**
 * @brief Write Digital Toggle Pin
 * @param gpioPort GPIO Port
 * @param gpioPin GPIO Pin
 */
void pinDigitalWriteToggle(GPIO_TypeDef* gpioPort, uint16_t gpioPin) {

	HAL_GPIO_TogglePin(gpioPort, gpioPin);
}
